<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Step4 extends CI_Controller {

	public function index()
	{
		$this->load->view('step4');
	}
	
	public function config()
	{
		$this->load->helper('file');
		//$string = read_file('./application/config/database.php');
		$Server=$this->input->post("server");
		$User=$this->input->post("username");
		$Pass=$this->input->post("password");
		$DB=$this->input->post("database");
		
		$data = 
			"
						 <?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

\$active_group = 'default';
\$active_record = TRUE;

\$db['default']['hostname'] = '$Server';
\$db['default']['username'] = '$User';
\$db['default']['password'] = '$Pass';
\$db['default']['database'] = '$DB';
\$db['default']['dbdriver'] = 'mysqli';
\$db['default']['dbprefix'] = '';
\$db['default']['pconnect'] = TRUE;
\$db['default']['db_debug'] = TRUE;
\$db['default']['cache_on'] = FALSE;
\$db['default']['cachedir'] = '';
\$db['default']['char_set'] = 'utf8';
\$db['default']['dbcollat'] = 'utf8_general_ci';
\$db['default']['swap_pre'] = '';
\$db['default']['autoinit'] = TRUE;
\$db['default']['stricton'] = FALSE; ";

		if ( ! write_file('../sideka/application/config/database.php', $data))
		{
			echo '<script>alert("Gagal Melakukan Konfigurasi");</script>';
		}
		else
		{
			echo '<script>alert("Konfigurasi Berhasil");</script>';
			$this->load->view('step5');
		}
	}
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */