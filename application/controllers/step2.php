<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Step2 extends CI_Controller {

	public function index()
	{
		$this->load->view('step2');
	}
	
	public function checkBox()
	{
		$this->load->library('unzip');
		$checked = $this->input->post('setuju');
		if($checked == 'yes'){
			$this->load->view('step3');
		
		}else{
			//echo '<script>alert("Silakan berikan check pada form");</script>';

		
			$this->load->view('step2');
		}
	}
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */