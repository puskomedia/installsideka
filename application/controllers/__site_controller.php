<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class site_controller extends CI_Controller {

	var $view;
	var $bahasa='indonesia';

	function __construct()
	{
		parent::__construct();
		$this->load->model('support_online_mdl');
		$this->view['support_online']=$this->support_online_mdl->get_list();

		$this->load->model('navigasi_mdl');
		$navigasi=$this->navigasi_mdl->get_list();
		$this->view['navigasi']=array();
		if(!empty($navigasi)){
			foreach ($navigasi as $val) {
				$this->view['navigasi'][]=array($val->url,$val->title,($val->target)?'target="_blank"':'');
			}
		}else{
			$this->view['navigasi']=array(
					array('profile','Profil',0),
					array('portofolio','Portofolio',0),
					array('ourteam','Our Team',0),
					array('ourcustomer','Our Customer',0),
					array('contact','Contact',0),
					array('career','Career',0),
				);
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */