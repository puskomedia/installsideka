<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Step6 extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('user_mdl');
		//$this->load->library('encrypt');
	}

	public function index()
	{
		$this->load->view('step6');
	}
	
	public function insert()
	{
		$password = $this->input->post('password');
		$confpassword = $this->input->post('confpass');
		if($password == $confpassword){
			echo '<script>alert("Konfigurasi Akun Administrator berhasil");</script>';
			// Setting Values For Tabel Columns
			$data = array(
			'nama_pengguna' => $this->input->post('username'),
			'password' => md5($this->input->post("password")),
			'nama' => '-',
			'no_telepon' => '-',
			'role' => 'Administrator',
			'foto' => '-',
			'nik' => '-'
			);
			$parsing['username'] = $this->input->post('username');
			$parsing['password'] = $this->input->post('password');
			// Transfering Data To Model
			$this->user_mdl->insert_admin($data);
			// Loading View
			$this->load->view('step7', $parsing);
		}else{
			echo '<script>alert("Konfirmasi Password tidak sesuai");</script>';
			$this->load->view('step6');
		}
	}
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */