<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Step3 extends CI_Controller {

	public function index()
	{
		$this->load->view('step3');
	}

	public function install()
	{
		$zip = new ZipArchive;
		if ($zip->open('Assets/sideka.zip') === TRUE) {
		    $zip->extractTo('/var/www/html');
		    $zip->close();
		    
		}

		/*$this->load->library('unzip');
		
		
		$this->unzip->allow(array('css', 'js', 'png', 'gif', 'jpeg', 'jpg', 'tpl', 'html', 'txt', 'php', 'xls', 'zip', 'htaccess', 'file', 'LGPL', 'AFM', 'ico', 'eot', 'otf', 'svg', 'ttf', 'woff', 'scss', 'less', 'md'));
		$this->unzip->extract('Assets/sideka.zip', 'C:/xampp/htdocs/');//C:\xampp\htdocs\InstallerWeb
		*/
		$folder_name = 'Assets';
		$file_name = 'sideka.sql';
		$path = '/var/www/html/installSideka/';
		$file_restore = $this->load->file($path . $folder_name . '/' . $file_name, true);
		$file_array = explode(';', $file_restore);
		foreach ($file_array as $query)
		{
			 if (trim($query)!=''){
				$this->db->query("SET FOREIGN_KEY_CHECKS = 0");
				$this->db->query($query);
				$this->db->query("SET FOREIGN_KEY_CHECKS = 1");
			}
		}
		$this->load->view('step5');
	}
}

/* End of file login.php */
/* Location: ./application/controllers/login.php */
