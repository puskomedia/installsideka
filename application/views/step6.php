<!DOCTYPE html>
<?php
	$template=base_url().'template/orangespot/';
	$bootstrap=$template.'bootstrap/';
?>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Selamat Datang di Aplikasi Instalasi Sistem Informasi Desa dan Kawasan</title>
	<link href="<?php echo $bootstrap?>css/bootstrap.min.css" rel="stylesheet" media="screen" />
	<script type="text/javascript" src="<?php echo $bootstrap?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo $bootstrap?>js/bootstrap.min.js"></script>
	<link href="<?php echo $template?>style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="docAll" class="container">
	<div id="atas" class="row">
	</div>

	<div class="row">
		<div class="span3">
			<br/>
			<a href="<?php echo base_url()?>" title="Home">
				<img src="<?php echo $template?>logo.gif" width="244" border="0" id="logo" />
			</a>
		</div>
		<div class="span9">
			<ul id="menu" class="nav nav-tabs">
			<h3>Step 6 out of 7 - Akun Administrator</h3>
			</ul>
		</div>
	</div>

	<div class="row">
		<div class="span3">
			<div id="leftmenu" style="padding-left:20px;">
			</div>
		</div>
		<div class="span9">
			<div id="isi">
			Database tables telah berhasil diisikan dengan data.
			Pengaturan akun administrator ini digunakan untuk membuat akun tertinggi dari sistem. 
			Atur pengguna pada halaman administrator menggunakan form ini.
			<br/><br/>
			<form class="m_bottom_45 m_xs_bottom_30" action="<?php echo site_url('step6/insert');?>" method="post">
				<div class="span4">
					<label for="input-judul">Username</label>
					<input class="input-block-level" type="text" placeholder="Username" required="required" name="username" id="username" value="<?php echo set_value('judul'); ?>">
					<br/>
					<label for="input-judul">Password</label>
					<input class="input-block-level" type="password" placeholder="Password" required="required" name="password" id="password" value="<?php echo set_value('judul'); ?>">
					<br/>
					<label for="input-judul">Confirm Password</label>
					<input class="input-block-level" type="password" placeholder="Confirm Password" required="required" name="confpass" id="confpass" value="<?php echo set_value('judul'); ?>">
				</div><br/><br/><br/><br/><br/><br/><br/><br/><br/>
			<br/><br/>
			<a href="<?php echo site_url('step5');?>" class="btn btn-primary">Back</a>
			<button type="submit" class="btn btn-primary">Next</button>
			</form>
			</div>
		</div>
	</div>

	<div class="clearfix"><br/></div>
	<div class="row">
		<div class="span3">
			<div id="copyright" class="well-small">&copy; Copyright 2015</div>
		</div>
		<div class="span9">
			<hr style="margin-bottom:9px;"/>
			<p class="muted"><small>
				<img src="<?php echo $template?>logo_footer.png" width="244" border="0" id="logo" />
			</small></p>
		</div>
	</div>
</div>
</body>
</html>