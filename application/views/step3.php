<!DOCTYPE html>
<?php
	$template=base_url().'template/orangespot/';
	$bootstrap=$template.'bootstrap/';
?>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Selamat Datang di Aplikasi Instalasi Sistem Informasi Desa dan Kawasan</title>
	<link href="<?php echo $bootstrap?>css/bootstrap.min.css" rel="stylesheet" media="screen" />
	<script type="text/javascript" src="<?php echo $bootstrap?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo $bootstrap?>js/bootstrap.min.js"></script>
	<link href="<?php echo $template?>style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="docAll" class="container">
	<div id="atas" class="row">
	</div>

	<div class="row">
		<div class="span3">
			<br/>
			<a href="<?php echo base_url()?>" title="Home">
				<img src="<?php echo $template?>logo.gif" width="244" border="0" id="logo" />
			</a>
		</div>
		<div class="span9">
			<ul id="menu" class="nav nav-tabs">
			<h3>Step 3 out of 5 - Pra Instalasi</h3>
			</ul>
		</div>
	</div>

	<div class="row">
		<div class="span3">
			<div id="leftmenu" style="padding-left:20px;">
			</div>
		</div>
		<div class="span9">
			<div id="isi">
			Sebelum melanjutkan seluruh proses pemasangan, Pastikan semua kebutuhan sistem terpenuhi.
			</div><br/><br/>
			<p style="color:red;">*jangan melakukan reload pada browser anda pada saat anda selesai menekan tombol install.</p>
			<form class="m_bottom_45 m_xs_bottom_30" action="<?php echo site_url('step3/install');?>" method="post">
				<button class="btn btn-primary" onclick="history.go(-1);">Kembali</button>
				<button type="submit" id="next"class="btn btn-primary">Install</button>
			</form>
		</div>
	</div>

	<div class="clearfix"><br/></div>
	<div class="row">
		<div class="span3">
			<div id="copyright" class="well-small">&copy; Copyright 2015</div>
		</div>
		<div class="span9">
			<hr style="margin-bottom:9px;"/>
			<p class="muted"><small>
				<img src="<?php echo $template?>logo_footer.png" width="244" border="0" id="logo" />
			</small></p>
		</div>
	</div>
</div>
</body>
</html>

<script>
$(document).ready(function(){

document.getElementById("next").disabled =false;
 $('form').submit(function() { 
						
			// always return false to prevent standard browser submit and page navigation 
			document.getElementById("next").disabled =true;

			return true; 
		}); 

});


</script>