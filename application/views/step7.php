<!DOCTYPE html>
<?php
	$template=base_url().'template/orangespot/';
	$bootstrap=$template.'bootstrap/';
?>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Selamat Datang di Aplikasi Instalasi Sistem Informasi Desa dan Kawasan</title>
	<link href="<?php echo $bootstrap?>css/bootstrap.min.css" rel="stylesheet" media="screen" />
	<script type="text/javascript" src="<?php echo $bootstrap?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo $bootstrap?>js/bootstrap.min.js"></script>
	<link href="<?php echo $template?>style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="docAll" class="container">
	<div id="atas" class="row">
	</div>

	<div class="row">
		<div class="span3">
			<br/>
			<a href="<?php echo base_url()?>" title="Home">
				<img src="<?php echo $template?>logo.gif" width="244" border="0" id="logo" />
			</a>
		</div>
		<div class="span9">
			<ul id="menu" class="nav nav-tabs">
			<h3>Step 5 out of 5 - Selesai</h3>
			</ul>
		</div>
	</div>

	<div class="row">
		<div class="span3">
			<div id="leftmenu" style="padding-left:20px;">
			</div>
		</div>
		<div class="span9">
			<div id="isi">
			Proses instalasi SIDeKa telah selesai.
			<br/>Aplikasi anda terpasang di <a href='http://localhost/sideka'>http://localhost/sideka</a>
			<br/>Untuk mengakses halaman admin, silakan klik <a href='http://localhost/sideka/c_login'>http://localhost/sideka/c_login</a>
			<br/>Terdapat dua role yang bisa anda gunakan untuk mengakses SIDeKa, yang pertama adalah administrator:
			<br/><br/>Username : sidekaadmin
			<br/>Password : sidekapass
			</br><br/>Yang kedua adalah pengelola data:
			<br/><br/>Username : sidekapengelola
			<br/>Password : sidekapass
			</div><br/><br/>
			<p style="color:red;">*Segera ganti password anda setelah masuk kedalam sistem.</p>
			
		</div>
	</div>

	<div class="clearfix"><br/></div>
	<div class="row">
		<div class="span3">
			<div id="copyright" class="well-small">&copy; Copyright 2015</div>
		</div>
		<div class="span9">
			<hr style="margin-bottom:9px;"/>
			<p class="muted"><small>
				<img src="<?php echo $template?>logo_footer.png" width="244" border="0" id="logo" />
			</small></p>
		</div>
	</div>
</div>
</body>
</html>