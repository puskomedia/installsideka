<!DOCTYPE html>
<?php
	$template=base_url().'template/orangespot/';
	$bootstrap=$template.'bootstrap/';
?>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Selamat Datang di Aplikasi Instalasi Sistem Informasi Desa dan Kawasan</title>
	<link href="<?php echo $bootstrap?>css/bootstrap.min.css" rel="stylesheet" media="screen" />
	<script type="text/javascript" src="<?php echo $bootstrap?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo $bootstrap?>js/bootstrap.min.js"></script>
	<link href="<?php echo $template?>style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="docAll" class="container">
	<div id="atas" class="row">
	</div>

	<div class="row">
		<div class="span3">
			<br/>
			<a href="<?php echo base_url()?>" title="Home">
				<img src="<?php echo $template?>logo.gif" width="244" border="0" id="logo" />
			</a>
		</div>
		<div class="span9">
			<ul id="menu" class="nav nav-tabs">
			<h3>Step 2 out of 5 - Lembar Persetujuan</h3>
			</ul>
		</div>
	</div>

	<div class="row">
		<div class="span3">
			<div id="leftmenu" style="padding-left:20px;">
			</div>
		</div>
		<div class="span9">
			<div id="isi">
			Selamat datang di lembar persetujuan "SIDEKA Sistem Informasi Desa dan Kawasan". Setujui persyaratan penggunaan software untuk menjadikan software resmi dipasang. Pada lembar bacalah persetujuan
			<br/><br/>
			1. LISENSI SIDEKA Opensource (Tidak Profit)
			<br/>SIDEKA Opensource adalah software tak berbayar (opensource) yang boleh digunakan oleh siapa saja selama bukan untuk kepentingan profit atau komersial. Lisensi ini mengizinkan setiap orang untuk menggubah, memperbaiki, dan membuat ciptaan turunan bukan untuk kepentingan komersial, selama merasa mencantumkan asal pembuat kepada Anda dan melisensikan ciptaan turunan dengan syarat yang serupa dengan ciptaan asli.
			<br/><br/>
			2. SYARAT DAN KETENTUAN
			<br/>Untuk mendapatkan SIDEKA Resmi, Anda diharuskan mengirimkan surat permohonan ataupun izin SIDEKA terlebih dahulu, aplikasi ini akan tetap bersifat opensource dan anda tidak dikenai biaya.
			<br/><br/>
			3. HAKI (Hak Kekayaan Intelektual)
			<br/>SIDEKA Opensource TIDAK BOLEH digunakan dengan tujuan profit atau segala usaha yang bertujuan untuk mencari keuntungan pelanggan HAKI(Hak Kekayaan Intelektual) merupakan tindakan yang menghancurkan dan menghambat karya bangsa.
			<br/><br/>
			4. TIDAK ADA GARANSI UNTUK PENGGUNAAN SIDEKA
			<br/>SIDEKA disediakan sebagai opensource, tanpa ada garansi. Kami tidak menjamin bahwa SIDEKA akan berjalan tanpa error, jadi segala bentuk kerusakan yang mungkin berhubungan dengan penggunaan SIDeKa adalah diluar tanggungjawab tim SIDeKa.
			<br/><br/>
			<form class="m_bottom_45 m_xs_bottom_30" action="<?php echo site_url('step2/checkbox');?>" method="post">
				<input type="checkbox" name="setuju" value="yes" id="setuju"> Saya setuju dan mengerti segala resikonya.
				</div><br/>
				<button class="btn btn-primary" onclick="history.go(-1);">Kembali</button>
				<button type="submit" class="btn btn-primary" id="next">Setuju</button>
			</form>
		</div>
	</div>

	<div class="clearfix"><br/></div>
	<div class="row">
		<div class="span3">
			<div id="copyright" class="well-small">&copy; Copyright 2015</div>
		</div>
		<div class="span9">
			<hr style="margin-bottom:9px;"/>
			<p class="muted"><small>
				<img src="<?php echo $template?>logo_footer.png" width="244" border="0" id="logo" />
			</small></p>
		</div>
	</div>
</div>
</body>
</html>

<script>
$(document).ready(function(){

document.getElementById("next").disabled =true;


});

$("#setuju").on("click",function(){

validate();

});
function validate() {
        if (document.getElementById('setuju').checked) {
            
document.getElementById("next").disabled =false;
        } else {
            
document.getElementById("next").disabled =true;
        }
    }

</script>