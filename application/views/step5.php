<!DOCTYPE html>
<?php
	$template=base_url().'template/orangespot/';
	$bootstrap=$template.'bootstrap/';
?>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title>Selamat Datang di Aplikasi Instalasi Sistem Informasi Desa dan Kawasan</title>
	<link href="<?php echo $bootstrap?>css/bootstrap.min.css" rel="stylesheet" media="screen" />
	<script type="text/javascript" src="<?php echo $bootstrap?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo $bootstrap?>js/bootstrap.min.js"></script>
	<link href="<?php echo $template?>style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div id="docAll" class="container">
	<div id="atas" class="row">
	</div>

	<div class="row">
		<div class="span3">
			<br/>
			<a href="<?php echo base_url()?>" title="Home">
				<img src="<?php echo $template?>logo.gif" width="244" border="0" id="logo" />
			</a>
		</div>
		<div class="span9">
			<ul id="menu" class="nav nav-tabs">
			<h3>Step 4 out of 5 - Pemasangan Telah Selesai</h3>
			</ul>
		</div>
	</div>

	<div class="row">
		<div class="span3">
			<div id="leftmenu" style="padding-left:20px;">
			</div>
		</div>
		<div class="span9">
			<div id="isi">
			Sekarang kita sudah siap untuk menyelesaikan proses instalasi. Sistem sudah selesai melakukan proses pemasangan database. 
			Seharusnya tidak ada masalah jika sesuai dengan pengaturan awal, kembali ke pengaturan database jika masih terjadi kesalahan.
			</div><br/><br/>
			<button class="btn btn-primary" onclick="history.go(-1);">Kembali</button>
			<a href="<?php echo site_url('step7');?>"><button class="btn btn-primary">Selanjutnya</button></a>
		</div>
	</div>

	<div class="clearfix"><br/></div>
	<div class="row">
		<div class="span3">
			<div id="copyright" class="well-small">&copy; Copyright 2015</div>
		</div>
		<div class="span9">
			<hr style="margin-bottom:9px;"/>
			<p class="muted"><small>
				<img src="<?php echo $template?>logo_footer.png" width="244" border="0" id="logo" />
			</small></p>
		</div>
	</div>
</div>
</body>
</html>