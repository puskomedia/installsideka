<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user_mdl extends CI_Model {

	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	function insert_admin($data)
	{
		$this->db->insert('tbl_pengguna', $data);
	}
}