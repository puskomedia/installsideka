<?php
$lang['passconf']  = 'Konfirmasi Password';
$lang['nama']      = 'Nama';
$lang['no_ktp']    = 'Nomor KTP';
$lang['no_telp']   = 'Nomor Telepon';
$lang['alamat']    = 'Alamat';
$lang['sebagai']   = 'Daftar Sebagai';
$lang['customer']  = 'Konsumen';
$lang['tos']       = 'saya setuju dengan <a href="#">Syarat dan Ketentuan</a> yang berlaku di website ini';
$lang['agreement'] = 'untuk bisa registrasi anda harus setuju dengan <a href="#">Syarat dan Ketentuan</a> yang berlaku di website ini';
